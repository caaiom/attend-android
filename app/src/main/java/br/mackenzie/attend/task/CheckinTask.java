package br.mackenzie.attend.task;

import android.content.ContentValues;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

/**
 * Created by cmaia on 10/10/16.
 */
public class CheckinTask extends AsyncTask<Map<String, String>, String, String> {

    @Override
    protected String doInBackground(Map<String, String>... map) {
        try {
            URL url = new URL("http://10.0.2.2:8080/api/checkin");

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);

            JSONObject json = new JSONObject();

            for (Map.Entry<String, String> entry : map[0].entrySet())
            {
                json.put(entry.getKey(), entry.getValue());
            }

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

            writer.write(json.toString());
            writer.flush();
            writer.close();

            os.close();
            conn.connect();

            return "Sent";
        } catch (IOException e) {
            // Do nothing
            return "Deu ruim";
        } catch (JSONException e) {
            // Do nothing
            return "Deu ruim";
        }
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        //Do anything with response..
    }
}
