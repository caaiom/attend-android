package br.mackenzie.attend.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by cmaia on 10/10/16.
 */
public class CheckIn implements Serializable {
    private static final long serialVersionUID = 2950774074904015315L;
    private String event;
    private String location;
    private Double latitude;
    private Double longitude;
    private Long userId;
    private Date checkInTime;

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Date getCheckInTime() {
        return checkInTime;
    }

    public void setCheckInTime(Date checkInTime) {
        this.checkInTime = checkInTime;
    }
}
